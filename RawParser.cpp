#include <string>
#include <sstream>
#include <arpa/inet.h>
#include <boost/detail/endian.hpp>
#include <boost/filesystem.hpp>
#include <libgen.h>
#include "RawParser.h"

extern "C" {
#include "jpeg_wrapper.h"
}

namespace fs = boost::filesystem;
using std::ios_base;
using std::stringstream;

namespace rawparser
{
RawParser::RawParser()
{
}

RawParser::~RawParser()
{
}

// private
unsigned short RawParser::bytesToUShort(bool isBigEndian, const array<unsigned char, 2> &buf)
{
    unsigned short val = 0;

#if defined(BOOST_LITTLE_ENDIAN)
    if (isBigEndian) {
        val = ntohs(*(unsigned short *)&buf[0]);
    } else {
#endif
        val = (buf[1] << 8) | (buf[0] & 0xFF);
#if defined(BOOST_LITTLE_ENDIAN)
    }
#endif
    return val;
}

unsigned int RawParser::bytesToUInt(bool isBigEndian, const array<unsigned char, 4> &buf)
{
    unsigned int val = 0;

#if defined(BOOST_LITTLE_ENDIAN)
    if (isBigEndian) {
        val = ntohl(*(unsigned int *)&buf[0]);
        //std::cout << "Val: " << val << std::endl;
    } else {
#endif
        val = *(unsigned int *)&buf[0];
#if defined(BOOST_LITTLE_ENDIAN)
    }
#endif
    return val;
}

void RawParser::bytesToASCIIString(vector<char> &bytes, string &outStr)
{
    outStr = (reinterpret_cast<char const *>(&bytes[0]), bytes.size());
}

int RawParser::readField(size_t offset, unsigned int bytesToRead, ifstream &is, vector<char> &bufOut)
{
    bufOut.resize(bytesToRead);
    is.seekg(offset, ios_base::beg);
    is.read(&bufOut[0], bytesToRead);

    return 0;
}

vector<IfdEntry> RawParser::processIfd(bool isBigEndian, size_t offset, ifstream &is)
{
    vector<IfdEntry> v;
    vector<char> buf;

    // entries
    readField(offset, 2, is, buf);

    auto entries = bytesToUShort(isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1]}});
    offset += 2;
    v.clear();

    for (int i = 0; i < entries; i++) {
        IfdEntry entry;

        // tag
        readField(offset, 2, is, buf);
        entry.tag = bytesToUShort(isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1]}});
        buf.clear();
        offset += 2;

        // type
        readField(offset, 2, is, buf);
        entry.fieldType = bytesToUShort(isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1]}});
        buf.clear();
        offset += 2;

        // count
        readField(offset, 4, is, buf);
        entry.count = bytesToUInt(isBigEndian,
        {{(unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3]}});
        buf.clear();
        offset += 4;

        // value offset
        readField(offset, 4, is, buf);
        entry.valueOffset = bytesToUInt(isBigEndian,
        {{(unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3]}});
        buf.clear();
        offset += 4;

        v.push_back(entry);
    }

    return v;
}

pair<float, float> RawParser::processRationalEntry(bool isBigEndian, size_t offset, ifstream &is)
{
    vector<char> buf;
    float r = 0.0;

    readField(offset, 4, is, buf);
    auto num = bytesToUInt(isBigEndian,
    {{(unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3]}});
    buf.clear();
    offset += 4;

    auto den = bytesToUInt(isBigEndian,
    {{(unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3]}});
    offset += 4;

    if (den != 0.0) {
        r = (float)num / (float)den;
    }

    return pair<float, float> {(float)num , r};
}

string RawParser::processASCIIEntry(IfdEntry &entry, ifstream &is)
{
    vector<char> buf;
    string val;

    readField(entry.valueOffset, entry.count, is, buf);

    bytesToASCIIString(buf, val);

    return val;
}

unsigned short RawParser::processShortValue(bool isBigEndian, unsigned int val)
{
    unsigned short result, msb, lsb;

#if defined(BOOST_LITTLE_ENDIAN)
    msb = (val >> 16);
    lsb = val & 0x0000FFFF;
#else
    msb = val & 0x0000FFFF;
    lsb = (val >> 16);
#endif

    if (isBigEndian) {
        result = msb;
    } else {
        result = lsb;
    }

    return result;
}

string RawParser::genExtractedJpegname(const string &fileName, const string &destDir, const string &suffix)
{
    boost::filesystem::path slash("/");
    string pathSep = slash.make_preferred().native();
    char *baseFile = basename((char *)fileName.c_str());

    //std::cout << "Pathsep: " << pathSep << " Base: " << baseFile << std::endl;

    stringstream ss;
    ss << destDir << pathSep << baseFile << suffix;

    //std::cout << "SS: " << ss.str() << std::endl;

    return ss.str();
}

string RawParser::decodeAndWriteJpeg(ifstream &is, string fileName, string &destDir, JpegInfo &j, int quality)
{
    static const string suffix = "_extracted.jpg";

    string jpegFileName = genExtractedJpegname(fileName, destDir, suffix);

    //std::cout << "jpegFileName: " << jpegFileName << std::endl;

    // read compressed jpeg bytes
    char *buf = (char *)malloc(j.length * sizeof(char));
    is.seekg(j.offset, ios_base::beg);
    is.read(buf, j.length);

    decodeEncodeWrite((unsigned char *)buf, j.length, quality, (char *)jpegFileName.c_str());

    free(buf);

    return jpegFileName;
}

} // namespace