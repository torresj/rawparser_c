#include <iostream>
#include <cmath>
#include "Cr2Parser.h"


namespace rawparser
{
Cr2Parser::Cr2Parser(): RawParser()
{

}
Cr2Parser::~Cr2Parser()
{

}

RawFile *Cr2Parser::processFile(RawFileInfo &ri)
{
    RawFile *cr2 = new RawFile();
    ifstream is;
    is.open(ri.file, ifstream::binary);
    if (!is.good()) {
        std::cout << "Error: Unable to open file: " << ri.file << std::endl;
        return 0;
    }

    //    std::cout << "Opened file: " << ri.file << std::endl;

    Cr2Header header = processHeader(is);
    JpegInfo jpegInfo = processIfds(is, header);

    // decode and write out jpeg
    auto jpegPath = decodeAndWriteJpeg(is, ri.file, ri.destDir, jpegInfo, ri.quality);

    cr2->fileName = ri.file;
    cr2->dateTime = jpegInfo.dateTime;
    cr2->jpegPath = jpegPath;
    cr2->jpegOrientation = jpegInfo.orientation;

    is.close();

    return cr2;
}

Cr2Header Cr2Parser::processHeader(ifstream &is)
{
    vector<char> buf;
    Cr2Header h;

    // byte order
    readField(0, 2, is, buf);
    auto byteOrder = bytesToUShort(false, {{(unsigned char)buf[0], (unsigned char)buf[1]}});
    buf.clear();
    h.isBigEndian = (byteOrder == 0x4D4D);

    // tiff magic value
    readField(2, 2, is, buf);
    auto tiffMagicVal = bytesToUShort(h.isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1]}});
    buf.clear();
    h.tiffMagicValue = tiffMagicVal;

    // tiff offset
    readField(4, 4, is, buf);
    auto tiffOffset = bytesToUInt(h.isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3]}});
    buf.clear();
    h.tiffOffset = tiffOffset;

    // cr2 magic val
    readField(8, 2, is, buf);
    bytesToASCIIString(buf, h.cr2MagicValue);
    buf.clear();

    // cr2 major num
    readField(10, 1, is, buf);
    buf.clear();
    h.cr2MajorValue = (unsigned char)buf[0];

    // cr2 minor num
    readField(11, 1, is, buf);
    buf.clear();
    h.cr2MinorValue = (unsigned char)buf[0];

    return h;
}

JpegInfo Cr2Parser::processIfds(ifstream &is, Cr2Header &h)
{
    JpegInfo jpeg;
    vector<char> buf;

    auto offset = h.tiffOffset;

    auto entries = processIfd(h.isBigEndian, offset, is);

    for (auto &entry : entries) {
        if (entry.tag == 0x0111) {
            // JPEG offset for IFD0
            jpeg.offset = entry.valueOffset;
        } else if (entry.tag == 0x0112) {
            // orientation tag
            unsigned short o = processShortValue(h.isBigEndian, entry.valueOffset);
            if (o == 8) {
                // rotate 270 CW
                jpeg.orientation = 270 * M_PI / 180;
            } else {
                jpeg.orientation = 0.0;
            }
        } else if (entry.tag == 0x0117) {
            jpeg.length = entry.valueOffset;
        } else if (entry.tag == 0x011A) {
            //jpeg.xRes
            pair<float, float> rat = processRationalEntry(h.isBigEndian, entry.valueOffset, is);
            jpeg.xRes = rat.first;
            jpeg.xResFloat = rat.second;
        } else if (entry.tag == 0x011B) {
            //jpeg.yRes
            pair<float, float> rat = processRationalEntry(h.isBigEndian, entry.valueOffset, is);
            jpeg.yRes = rat.first;
            jpeg.yResFloat = rat.second;
        } else if (entry.tag == 0x8769) {
            // EXIF IFD Pointer
            // EXIF IFD pointer.  Note: the pointer is the value represented
            // in valueOffset.

            // Read EXIF entries
            auto exifEntries = processIfd(h.isBigEndian, entry.valueOffset, is);
            for (auto &exifEntry : exifEntries) {
                if (exifEntry.tag == 0x9004) {
                    auto createDate = processASCIIEntry(exifEntry, is);
                    jpeg.dateTime = createDate;
                    // parseDataTime(createDate);
                }
            }
        }
    }

    return jpeg;
}

} // namespace