#include <iostream>
#include <array>
#include <vector>
#include <map>
#include <chrono>
#include <string>
#include <thread>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/filesystem.hpp>
#include <boost/progress.hpp>
#include <boost/algorithm/string.hpp>
#include "NefParser.h"
#include "Cr2Parser.h"

using std::array;
using std::cout;
using std::endl;
using std::map;
using std::string;
using std::vector;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::minutes;
using std::chrono::steady_clock;
using namespace rawparser;

namespace fs = ::boost::filesystem;
namespace po = boost::program_options;
namespace ba = boost::algorithm;

// constants
static const string CR2_PARSER_KEY = "CR2";
static const string NEF_PARSER_KEY = "NEF";
// Array defines the RAW camera types supported
array<string, 2> RAW_TYPES = {CR2_PARSER_KEY, NEF_PARSER_KEY};

// globals
unsigned int g_processed;
boost::progress_display *g_showProgress;
map<string, RawParser *> g_parserMap;

typedef struct {
    string destDir;
    int quality;
    vector<string> srcDirs;
    vector<string> rawTypes;
} ProcessingArgs;

// Based on http://stackoverflow.com/questions/11140483/how-to-get-list-of-files-with-a-specific-extension-in-a-given-folder
// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
void get_all(const fs::path &root, const string &ext, vector<fs::path> &ret)
{
    if (!fs::exists(root)) {
        return;
    }

    if (fs::is_directory(root)) {
        fs::recursive_directory_iterator it(root);
        fs::recursive_directory_iterator endit;
        while (it != endit) {
            if (fs::is_regular_file(*it) and it->path().extension() == ext) {
                ret.push_back(fs::absolute(it->path(), root));//filename());
            }
            ++it;
        }
    }
}

void updateProgress()
{
    ++*g_showProgress;
}

inline void printUsage(po::options_description &desc, string option, bool fail = true, string errorMsg = "")
{
    if (fail) {
        cout << "Missing required option: '" << option  << "'" << endl;
        if (errorMsg != "") {
            cout << "Error: " << errorMsg << endl;
        }
        cout << desc << endl;
        exit(1);
    } else {
        cout << desc << endl;
    }
}

inline void buildParserMap()
{
    // add supported parsers by utility
    g_parserMap[NEF_PARSER_KEY] = new NefParser();
    g_parserMap[CR2_PARSER_KEY] = new Cr2Parser();
}

inline void cleanup()
{
    delete g_showProgress;

    // delete instaniated parsers
    for (auto &kv : g_parserMap) {
        if (kv.second) {
            delete kv.second;
        }
    }
}

inline bool isRawTypeValid(string &rawType)
{
    bool isValid = false;
    for (auto &raw : RAW_TYPES) {
        if (raw == rawType) {
            isValid = true;
            break;
        }
    }
    return isValid;
}

inline bool isDirValid(string &pathStr)
{
    fs::path path(pathStr);
    return (fs::exists(path) && fs::is_directory(path));
}

void parseArgs(int argc, char *argv[], ProcessingArgs &args)
{
    // process command line (using boost library)
    string rawsStr, srcDirsStr, destDirStr;

    po::options_description desc("Usage");
    desc.add_options()
    ("help", "produce help message")
    ("raws", po::value< vector<string> >(), "the (comma-separated) RAW file types to process.  Currently NEF or CR2")
    ("quality", po::value<int>(&args.quality)->default_value(75), "set JPEG compression level")
    ("src-dirs", po::value< vector<string> >(), "the source directories containing RAW files")
    ("dest-dir", po::value< vector<string> >(), "the destination directory for extracted JPEGs");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        printUsage(desc, "help");
    }

    if (vm.count("raws")) {
        vector<string> raws = vm["raws"].as< vector<string> >();
        if (raws.size() > 0) {
            rawsStr = raws.at(0);
            boost::trim(rawsStr);
            ba::split(args.rawTypes, rawsStr, ba::is_any_of(",|"), ba::token_compress_on);

            vector<string> copy;
            // trim tokens
            cout << "RAW types: [ ";
            for (auto &raw : args.rawTypes) {
                boost::trim(raw);
                copy.push_back(raw);
                cout << raw << " ";

                // verify if valid raw type
                if (!isRawTypeValid(raw)) {
                    printUsage(desc, "raws");
                }
            }
            cout << "]" << endl;

            args.rawTypes = copy;
        }
    } else {
        printUsage(desc, "raws");
    }

    if (vm.count("src-dirs")) {
        vector<string> srcDirsInput = vm["src-dirs"].as< vector<string> >();
        if (srcDirsInput.size() > 0) {
            srcDirsStr = srcDirsInput.at(0);
            boost::trim(srcDirsStr);
            ba::split(args.srcDirs, srcDirsStr, ba::is_any_of(",|"), ba::token_compress_on);

            vector<string> copy;
            for (auto &dir : args.srcDirs) {
                boost::trim(dir);

                // don't continue if invalid dirs
                if (!isDirValid(dir)) {
                    printUsage(desc, "src-dirs", true, "Invalid source directory");
                }

                copy.push_back(dir);
            }
            args.srcDirs = copy;
        }
    } else {
        printUsage(desc, "src-dirs");
    }

    if (vm.count("dest-dir")) {
        vector<string> destDirInput = vm["dest-dir"].as< vector<string> >();
        if (destDirInput.size() > 0) {
            destDirStr = destDirInput.at(0);
            boost::trim(destDirStr);

            // don't continue if invalid dirs
            if (!isDirValid(destDirStr)) {
                printUsage(desc, "dest-dir", true, "Invalid destination directory");
            }

            cout << "Dest dir: " << destDirStr << endl;
            args.destDir = destDirStr;
        }
    } else {
        printUsage(desc, "dest-dir");
    }

    if (vm.count("quality")) {
        cout << "JPEG compression level was set to "
             << vm["quality"].as<int>() << ".\n";
    } else {
        cout << "Using defulat JPEG compression level.\n";
    }
}

void doProcess(ProcessingArgs &args)
{
    map<string, vector<fs::path>*> rawMap;
    size_t fileCnt = 0;

    // get all files by RAW type
    for (auto &rootStr : args.srcDirs) {
        cout << "Getting files for dir: " << rootStr << endl;
        fs::path root(rootStr);
        vector<fs::path> *files;

        // raw files by type
        for (auto &rawType : args.rawTypes) {
            if (rawMap.find(rawType) == rawMap.end()) {
                // use heap, as we may have a lot of files to process
                files = new vector<fs::path>();
                rawMap[rawType] = files;
            } else {
                // add files to existing vector
                files = rawMap[rawType];
            }

            // get files for RAW type for directory
            get_all(root, "." + rawType, *files);
        }
    }

    for (auto &kv : rawMap) {
        fileCnt += kv.second->size();
    }

    // process files
    g_processed = 0;

    cout << "RawParser processing a total of: " << fileCnt << " files." << endl;

    // fire up progress display
    g_showProgress = new boost::progress_display(fileCnt);

    // raw files by type
    for (auto &kv : rawMap) {

        string rawType = kv.first;

        RawParser *parser = g_parserMap[rawType];

        // get files for raw type extension
        auto files = kv.second;

        size_t rawTypeCnt = files->size();

        #pragma omp parallel
        {
            #pragma omp for
            for (size_t i = 0; i < rawTypeCnt; i++) {
                fs::path p = files->at(i);

                RawFileInfo info;
                info.file = p.string();
                info.destDir = args.destDir;
                info.quality = args.quality;

                RawFile *f = parser->processFile(info);
                if (f) {
#if DEBUGPRINT
                    cout << "Parsed RAW file.  FileName: " << f->fileName
                         << " DateTime: " << f->dateTime
                         << " jpegPath: " << f->jpegPath
                         << " jpegOriention: " << f->jpegOrientation << endl;
#endif
                    delete f;
                }

                #pragma omp atomic
                g_processed++;

                #pragma omp critical
                updateProgress();
            }
        }
    }
}

int main(int argc, char *argv[])
{
    ProcessingArgs args;

    parseArgs(argc, argv, args);

    buildParserMap();

    steady_clock::time_point start = steady_clock::now();

    doProcess(args);

    steady_clock::time_point end = steady_clock::now();
    cout << "Processing Complete: [ "
         // duration_cast is required to avoid accidentally losing precision.
         << duration_cast<minutes>(end - start).count()
         << " minutes "
         << duration_cast<seconds>(end - start).count()
         << " seconds ("
         << duration_cast<milliseconds>(end - start).count()
         << " millis) ]"
         << endl;

    cleanup();

    return 0;
}
