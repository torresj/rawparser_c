#include <iostream>
#include <cmath>
#include "NefParser.h"


namespace rawparser
{
NefParser::NefParser(): RawParser()
{

}
NefParser::~NefParser()
{

}

RawFile *NefParser::processFile(RawFileInfo &ri)
{
    RawFile *nef = new RawFile();
    ifstream is;
    is.open(ri.file, ifstream::binary);
    if (!is.good()) {
        std::cout << "Error: Unable to open file: " << ri.file << std::endl;
        return 0;
    }

    //    std::cout << "Opened file: " << ri.file << std::endl;

    NefHeader header = processHeader(is);
    JpegInfo jpegInfo = processIfds(is, header);

    // decode and write out jpeg
    auto jpegPath = decodeAndWriteJpeg(is, ri.file, ri.destDir, jpegInfo, ri.quality);

    nef->fileName = ri.file;
    nef->dateTime = jpegInfo.dateTime;
    nef->jpegPath = jpegPath;
    nef->jpegOrientation = jpegInfo.orientation;

    is.close();

    return nef;
}

NefHeader NefParser::processHeader(ifstream &is)
{
    vector<char> buf;
    NefHeader h;

    // byte order
    readField(0, 2, is, buf);
    auto byteOrder = bytesToUShort(false, {{(unsigned char)buf[0], (unsigned char)buf[1]}});
    buf.clear();
    h.isBigEndian = (byteOrder == 0x4D4D);

    // tiff magic value
    readField(2, 2, is, buf);
    auto tiffMagicVal = bytesToUShort(h.isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1]}});
    buf.clear();
    h.tiffMagicValue = tiffMagicVal;

    // tiff offset
    readField(4, 4, is, buf);
    auto tiffOffset = bytesToUInt(h.isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3]}});
    h.tiffOffset = tiffOffset;

    return h;
}

JpegInfo NefParser::processIfds(ifstream &is, NefHeader &h)
{
    JpegInfo jpeg;
    vector<char> buf;

    auto offset = h.tiffOffset;

    auto entries = processIfd(h.isBigEndian, offset, is);

    for (auto &entry : entries) {
        if (entry.tag == 0x014A) {
            // JPEG offset (SUBID 0)
            readField(entry.valueOffset, 4, is, buf);
            auto subID0Offset = bytesToUInt(h.isBigEndian, {{(unsigned char)buf[0], (unsigned char)buf[1], (unsigned char)buf[2], (unsigned char)buf[3]}});
            buf.clear();

            // Read SUBIFD 0 for JPEG
            auto subIfd0Entries = processIfd(h.isBigEndian, subID0Offset, is);

            for (auto &subID0Entry : subIfd0Entries) {
                if (subID0Entry.tag == 0x011A) {
                    //jpeg.xRes
                    pair<float, float> rat = processRationalEntry(h.isBigEndian, subID0Entry.valueOffset, is);
                    jpeg.xRes = rat.first;
                    jpeg.xResFloat = rat.second;
                }
                if (subID0Entry.tag == 0x011B) {
                    //jpeg.xRes
                    pair<float, float> rat = processRationalEntry(h.isBigEndian, subID0Entry.valueOffset, is);
                    jpeg.yRes = rat.first;
                    jpeg.yResFloat = rat.second;
                }
                if (subID0Entry.tag == 0x0201) {
                    jpeg.offset = subID0Entry.valueOffset;
                }
                if (subID0Entry.tag == 0x0202) {
                    jpeg.length = subID0Entry.valueOffset;
                }
            }
        } else if (entry.tag == 0x0112) {
            // orientation tag
            auto o = processShortValue(h.isBigEndian, entry.valueOffset);
            if (o == 8) {
                // rotate 270 CW
                jpeg.orientation = 270 * M_PI / 180;
            } else {
                jpeg.orientation = 0.0;
            }

        } else if (entry.tag == 0x8769) {
            // EXIF IFD Pointer
            // EXIF IFD pointer.  Note: the pointer is the value represented
            // in valueOffset.

            // Read EXIF entries
            auto exifEntries = processIfd(h.isBigEndian, entry.valueOffset, is);
            for (auto &exifEntry : exifEntries) {
                if (exifEntry.tag == 0x9004) {
                    auto createDate = processASCIIEntry(exifEntry, is);
                    jpeg.dateTime = createDate;
                    // parseDataTime(createDate);
                }
            }
        }
    }

    return jpeg;
}

} // namespace