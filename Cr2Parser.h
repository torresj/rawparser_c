#ifndef CR2PARSER_H
#define CR2PARSER_H

#include "RawParser.h"

namespace rawparser
{
struct Cr2Header {
    bool isBigEndian;
    unsigned short tiffMagicValue;
    string cr2MagicValue;
    unsigned char cr2MajorValue, cr2MinorValue;
    size_t tiffOffset;
};

class Cr2Parser : public RawParser
{
public:
    Cr2Parser();
    virtual ~Cr2Parser();
    virtual RawFile *processFile(RawFileInfo &);
private:
    Cr2Header processHeader(ifstream &);
    JpegInfo processIfds(ifstream &, Cr2Header &);
};
}
#endif