#ifndef RAWPARSER_H
#define RAWPARSER_H

#include <array>
#include <string>
#include <vector>
#include <fstream>

using std::array;
using std::ifstream;
using std::string;
using std::vector;
using std::pair;

namespace rawparser
{

struct IfdEntry {
    unsigned short tag;
    unsigned short fieldType;
    unsigned int count;
    unsigned int valueOffset;
};

struct JpegInfo {
    float orientation;
    float xResFloat;
    float yResFloat;
    size_t offset;
    size_t length;
    unsigned int xRes;
    unsigned int yRes;
    string dateTime;
};

struct RawFileInfo {
    string file;
    string destDir;
    int quality;
};

struct RawFile {
    string dateTime;
    string fileName;
    string jpegPath;
    float jpegOrientation;
};

class RawParser
{
public:

    RawParser();
    virtual ~RawParser();
    virtual RawFile *processFile(RawFileInfo &) = 0;

protected:
    unsigned short bytesToUShort(bool, const array<unsigned char, 2> &);
    unsigned int bytesToUInt(bool, const array<unsigned char, 4> &);
    void bytesToASCIIString(vector<char> &, string &);
    string processASCIIEntry(IfdEntry &, ifstream &);
    int readField(size_t, unsigned int, ifstream &, vector<char> &);
    vector<IfdEntry> processIfd(bool, size_t, ifstream &);
    pair<float, float> processRationalEntry(bool, size_t, ifstream &);
    unsigned short processShortValue(bool, unsigned int);
    string decodeAndWriteJpeg(ifstream &is, string fileName, string &destDir, JpegInfo &jpegInfo, int quality);
    string genExtractedJpegname(const string &fileName, const string &destDir, const string &suffix);
};
} // namespace
#endif