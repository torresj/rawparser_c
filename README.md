# rawparser

## Overview
rawparser is a command-line utility to extract the embedded JPEGs from a camera RAW file.  It's a C++ language command line utility based on [RawParser](https://github.com/jeremytorres/rawparser).  There are existing tools that perform this or similar functionality; however, the reasons for creating this tool:

1. I have many RAW files that are processed using commercial software, yet on occassion, I would like the camera-produced JPEG for comparison.
2. To utilize the concurrency model provided by [OpenMP](http://openmp.org) language to process multiple files without any explicit "traditional" locking (e.g, mutexes)

### Current Status
* Currently only tested on MacOS X (10.9)

## Dependencies
* gcc compiler (built and tested with GCC 4.9) with _openmp_ support
* [Boost](http://www.boost.org)
* [libjpeg](http://www.ijg.org)
* [TurboJpeg](http://www.libjpeg-turbo.org/)
* If you have many JPEGs to extract, TurboJpeg provides noticebly better performance.
 
## Build
* Obtain the utility source.
 
```bash
git clone ssh://git@bitbucket.org:torresj/rawparser_c.git
cd rawparser_c
make
# 'rawparser' executable will be created
```

## Invoke
* Get help.  Displays information on parameters and defaults.
```bash
./rawparser --help
```

* Execute utility.

```bash
./rawparser --raws "CR2,NEF" --src-dirs "/path_to/raw_files/dir1,/path_to/raw_file/dir" --dest-dir "/path_to/extract_dir" --quality 75
```

* Verify output.  Extracted JPEGs will contain the source RAW file name with an "_extracted.jpg" extension.

### `--quality` parameter
The quality parameter is the JPEG compressors "quaility" value.  This is a value from 0-100, where 0 is the lowest compression quality.  This value has a dramatic impact on overall processing time and the size of the resulting extracted JPEG.  A good rule of thumb: 80 or less for really good JPEGs.  If storage size is of concern, us a smaller value.

### Current RAW file format support
* Nikon NEF
    * Nikon D700
* Canon CR2
    * Canon 5D MarkII
    * Canon 1D MarkIII

### Current Development Status
- I consider the current status a beta version as there is a laundry list of this I will like to support:
    - Add performance benchmarks
    - Add additional camera RAW file support
    - Create a "better" parser interface
