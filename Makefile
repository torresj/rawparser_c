#CXX = clang++
#CC = clang
CXX = g++-4.9
CC = gcc-4.9
CXXFLAGS = -Wall -std=c++11 -O3 -fopenmp -Wno-unused-local-typedefs
CFLAGS = -Wall -std=c99 -O4
LFLAGS = -L./lib/boost/boost_1_55_0/stage/lib -L/usr/local/opt/jpeg-turbo/lib
INCLUDES = -I. -I./lib/boost/boost_1_55_0
LIBS = -lturbojpeg -lboost_system -lboost_filesystem -lboost_program_options -lboost_thread
SRCS = RawParser.cpp NefParser.cpp Main.cpp Cr2Parser.cpp
OBJS = $(SRCS:.cpp=.o)
C_SRCS = jpeg_wrapper.c
C_OBJS = $(C_SRCS:.c=.o)
MAIN = rawparser


.PHONY: clean analyze

all: $(OBJS) $(C_OBJS)
	$(CXX) $(CXXFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) $(C_OBJS) $(LFLAGS) $(LIBS)

.cpp.o:
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $< -o $@

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

analyze: $(SRCS)
	$(CXX) $(CXXFLAGS) -fsyntax-only $(SRCS)

clean:
	-rm *.o *~ $(MAIN)
