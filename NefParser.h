#ifndef NEFPARSER_H
#define NEFPARSER_H

#include "RawParser.h"

namespace rawparser
{
struct NefHeader {
    bool isBigEndian;
    unsigned short tiffMagicValue;
    size_t tiffOffset;
};

class NefParser : public RawParser
{
public:
    NefParser();
    virtual ~NefParser();
    virtual RawFile *processFile(RawFileInfo &);
private:
    NefHeader processHeader(ifstream &);
    JpegInfo processIfds(ifstream &, NefHeader &);
};
}
#endif